///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file nunu.hpp
/// @version 1.0
///
/// Exports data about all nunu fish
///
/// @author @Andee Gary <andeeg@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   @todo 18_Feb_2021
///////////////////////////////////////////////////////////////////////////////
#pragma once

#include <string>

#include "fish.hpp"

using namespace std;

namespace animalfarm {

class Nunu : public Fish {
public: 
      bool Native;
      
      Nunu( bool  isNative, enum Color newColor, enum Gender newGender );
      
      void printInfo();
};

}

