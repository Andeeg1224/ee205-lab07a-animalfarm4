###############################################################################
# University of Hawaii, College of Engineering
# EE 205  - Object Oriented Programming
# Lab 07a - Animal Farm 4
#
# @file    Makefile
# @version 1.0
#
# @author @Andee Gary <andeeg@hawaii.edu>
# @brief  Lab 07a - Animal Farm 4 - EE 205 - Spr 2021
# @date   @25_Mar_2021
###############################################################################

all: main

main.o:  animal.hpp main.cpp
	g++ -c main.cpp

animal.o: animal.hpp animal.cpp node.hpp
	g++ -c animal.cpp
	
mammal.o: mammal.hpp mammal.cpp animal.hpp
	g++ -c mammal.cpp

cat.o: cat.cpp cat.hpp mammal.hpp
	g++ -c cat.cpp

dog.o: dog.cpp dog.hpp mammal.hpp
	g++ -c dog.cpp

fish.o: fish.cpp fish.hpp animal.hpp
	g++ -c fish.cpp

nunu.o: nunu.cpp nunu.hpp fish.hpp
	g++ -c nunu.cpp

aku.o: aku.cpp aku.hpp fish.hpp
	g++ -c aku.cpp

bird.o: bird.cpp bird.hpp animal.hpp
	g++ -c bird.cpp

palila.o: palila.cpp palila.hpp bird.hpp
	g++ -c palila.cpp

nene.o: nene.cpp nene.hpp bird.hpp
	g++ -c nene.cpp

main: main.cpp *.hpp main.o animal.o mammal.o cat.o dog.o fish.o nunu.o aku.o bird.o palila.o nene.o animalfactory.o node.o list.o
	g++ -o main main.o animal.o mammal.o cat.o dog.o fish.o nunu.o aku.o bird.o palila.o nene.o animalfactory.o list.o node.o

animalfactory.o: animalfactory.cpp animalfactory.hpp animal.hpp dog.hpp cat.hpp nunu.hpp aku.hpp palila.hpp nene.hpp 
	g++ -c animalfactory.cpp

test: test.cpp *.hpp test.o animal.o mammal.o cat.o dog.o fish.o nunu.o aku.o bird.o palila.o nene.o animalfactory.o
	g++ -o test test.o animal.o mammal.o cat.o dog.o fish.o nunu.o aku.o bird.o palila.o nene.o animalfactory.o

node.o: node.hpp node.cpp
	g++ -c node.cpp

list.o: list.hpp list.cpp
	g++ -c list.cpp

test2.o: node.hpp list.hpp animalfactory.hpp test2.cpp
	g++ -c test2.cpp

test2: test2.cpp *.hpp test2.o node.o list.o animalfactory.o
	g++ -o test2 test2.o node.o list.o

clean:
	rm -f *.o main test test2
