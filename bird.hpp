///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file bird.hpp
/// @version 1.0
///
/// Exports data about all birds
///
/// @author @Andee Gary <andeeg@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   @todo 18_Feb_2021
///////////////////////////////////////////////////////////////////////////////
#pragma once

#include <string>

#include "animal.hpp"

namespace animalfarm {

   class Bird : public Animal {
      public:
         bool  migratory;
         enum Color featherColor;
         bool isMigratory;
         virtual const string speak();
         void printInfo();
   };

}
