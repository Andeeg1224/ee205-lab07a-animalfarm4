///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// @file test2.cpp
/// @version 1.0
///
/// Test file for animalfarm4
///
/// @author @Andee Gary <andeeg@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   @25_Mar_2021
//////////////////////////////////////////////////////////////////////////////
#include <iostream>
#include "animal.hpp"
#include "node.hpp"
#include "list.hpp"
#include "animalfactory.hpp"

using namespace std;
using namespace animalfarm;

int main(){
   cout << "Hello World" << endl;
   Node node; //Instantiate a node
   SingleLinkedList list; //Instaniate a SingleLinkedList
  
   Node* test1 = new Node;
   Node* test2 = new Node;
   Node* test3 = new Node;
   cout << "The list is empty before putting in 3 tests: " << boolalpha << list.empty()  << endl;
   cout << "Test1 is: " << test1 << "  Test2 is: " << test2 << "  Test3 is: " << test3 << endl;

   list.push_front(test1);
   cout << "The initial front of list is: " << list.get_first() << endl;
   list.push_front(test2);
   list.push_front(test3);
   cout << "The list is still empty after inputting 3 tests: " << boolalpha << list.empty() << endl;

   cout << "The first node from the list after pushing test 2 then test 3 to the front is: " << list.get_first() << endl;
   cout << "The node following the node test 2 is: " << list.get_next ( test2 ) << endl;
   cout << "Amount of nodes in the list are: " << list.size() << endl;

   list.pop_front();
   cout << "Amount of nodes after using pop_front is: " << list.size() << endl;
}//End Main

