///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file nene.cpp
/// @version 1.0
///
/// Exports data about all nene birds
///
/// @author @Andee Gary <andeeg@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   @todo 18_Feb_2021
///////////////////////////////////////////////////////////////////////////////
#include <string>
#include <iostream>

#include "nene.hpp"

using namespace std;

namespace animalfarm {

Nene::Nene( string tagID, enum Color newColor, enum Gender newGender ){
   gender = newGender;
   species = "Branta sandvicensis";
   featherColor = newColor;
   isMigratory = true;
   id = tagID;
}

void Nene::printInfo(){
   cout << "Nene" << endl;
   cout << "   Tag ID = [" << id << "]" << endl;
   Bird::printInfo();
}

const string Nene::speak(){
   return string( "Nay, nay");
}
}
