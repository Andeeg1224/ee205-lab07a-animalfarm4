///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// @file list.cpp
/// @version 1.0
///
/// cpp file for List
///
/// @author @Andee Gary <andeeg@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   @25_Mar_2021
//////////////////////////////////////////////////////////////////////////////
#include <iostream>
#include "list.hpp"

using namespace std;

namespace animalfarm {
   
   const bool SingleLinkedList::empty() const{
      if ( head == nullptr )
         return true;
      else 
         return false;
      }
   void SingleLinkedList::push_front( Node* newNode ){
      if ( newNode == nullptr )
         return; //If no New Nodd just exit
      
      newNode->next = head;
      head = newNode;
      }
   Node* SingleLinkedList::pop_front(){
      if ( head == nullptr )
         return nullptr;

      Node* popFront = head;

      head = head ->next;

      return popFront;
   }
   Node* SingleLinkedList::get_first() const {
      if ( head == nullptr )
         return nullptr;
      else
         return head;
   }
   Node* SingleLinkedList::get_next( const Node* currentNode ) const {
      if ( currentNode -> next == nullptr )
         return nullptr;
      else
         return currentNode -> next;
   }
   unsigned int SingleLinkedList::size() const {
      unsigned int count = 0;
      Node* currentNode = head; 
      while ( currentNode != nullptr ) {
         currentNode = currentNode -> next;
         count++;
      }
      return count;
   }
}//Animal farm namespace

