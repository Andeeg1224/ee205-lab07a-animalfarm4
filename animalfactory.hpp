
///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file animalfactory.hpp
/// @version 1.0
///
/// animalfactory header
///
/// @author @Andee Gary <andeeg@hawaii.edu>
/// @brief  Lab 06a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   @09_Mar_2021
///////////////////////////////////////////////////////////////////////////////
#pragma once
using namespace std;

#include <string>

namespace animalfarm {

class AnimalFactory {
   public: 
      static Animal* getRandomAnimal();
};

} //end animalfarm namespace
